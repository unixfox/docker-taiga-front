FROM dockertaiga/front

WORKDIR /srv/taiga

RUN apk add --no-cache curl python3 python3-dev py-pip

RUN mkdir -p /srv/taiga/front/dist/plugins/slack/
RUN SLACK_VERSION=$(pip3 show taiga-contrib-slack | awk '/^Version: /{print $2}') && \
    echo "taiga-contrib-slack version: $SLACK_VERSION" && \
    curl https://raw.githubusercontent.com/taigaio/taiga-contrib-slack/$SLACK_VERSION/front/dist/slack.js -o /srv/taiga/front/dist/plugins/slack/slack.js && \
    curl https://raw.githubusercontent.com/taigaio/taiga-contrib-slack/$SLACK_VERSION/front/dist/slack.json -o /srv/taiga/front/dist/plugins/slack/slack.json

COPY config.json /tmp/taiga-conf/

VOLUME ["/taiga-conf"]

CMD ["/start.sh"]
